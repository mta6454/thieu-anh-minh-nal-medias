//var function menu click
$(function () {
  $(document).ready(function(){
    if($('.js-menu-expand').length > 0){
      $('.js-menu-expand').click(function(e) {
        e.preventDefault();
        var menu = $('.c-menu-wrapper');
        if (menu.hasClass('has-menu-tiny')) {
          $(this).removeClass('active');
          menu.removeClass('has-menu-tiny')
              .slideUp();
        } else {
          $(this).addClass('active');
          menu.addClass('has-menu-tiny')
              .slideDown();
        }
      });
    }
  });
});

//var function demo expand
$(function () {
  $(document).ready(function(){
    if($('.c-knowledge-item').length > 0){
      $('.js-knowledge-expand').click(function(e) {
        e.preventDefault();
        var parent = $(this).parent();
        var root = $(this).parent().parent().parent();
        if (parent.hasClass('active')) {
          parent.removeClass('active');
          $('.c-knowledge-item__detail', parent).slideUp();
        } else {
          $('.c-knowledge-item.active .c-knowledge-item__detail', root).slideUp();
          $('.c-knowledge-item', root).removeClass('active');
          parent.addClass('active');
          $('.c-knowledge-item__detail', parent).slideDown();
        }
      });
    }
  });
});

//var function demo slider
$(function () {
  $(document).ready(function(){
    if($('.js-class-slider-3').length > 0){
      $('.js-class-slider-3').owlCarousel({
        loop:false,
        margin:0,
        responsiveClass:false,
        nav:false,
        dots:false,
        autoplay:false,
        autoHeight:false,
        autoplayTimeout:6000,
        autoplaySpeed:1000,
        autoplayHoverPause:true,
        navText:false,
        responsive:{
          0:{
            items:2
          },
          768:{
            items:3
          }
        }
      });
    }
  });
});

$(function () {
  $(document).ready(function(){
    if($('.js-img-slider-3').length > 0){
      $('.js-img-slider-3').owlCarousel({
        loop:false,
        margin:0,
        responsiveClass:false,
        nav:false,
        dots:false,
        autoplay:false,
        autoHeight:false,
        autoplayTimeout:6000,
        autoplaySpeed:1000,
        autoplayHoverPause:true,
        navText:false,
        responsive:{
          0:{
            items:2
          },
          768:{
            items:2,
            margin:10
          },
          992:{
            items:3,
            margin:20
          },
          1200:{
            items:3,
            margin:30
          }

        }
      });
    }
  });
});

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  console.log('tapp')
  showSlides(slideIndex += n);
}

function showSlides(n) {
  var i;
  var x = document.getElementsByClassName("slide-item");
  if (n > x.length) {slideIndex = 1}
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}