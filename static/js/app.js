var localApi = "http://localhost:3000/"
// header fetch
fetch(localApi+"header")
.then(function(res) {
    return res.json();
}).then(
    function(data) {
    document.getElementById('header-1').innerHTML = `<div>${data.logo_title}<div>`;
    document.getElementById('header-2').innerHTML = `<div>${data.logo_content}<div>`;
   
    }
).catch(function(err){
    alert(err)
} )
// menu-bar fetch
fetch(localApi+"menu-bar").then(
function(res){
    return res.json();
}
).then(
    function(data) {
        document.getElementById('menu-1').innerHTML = `<a href='#'>${data.item_1}</a>`;
        document.getElementById('menu-2').innerHTML = `<a href='#'>${data.item_2}</a>`;
        document.getElementById('menu-3').innerHTML = `<a href='#'>${data.item_3}</a>`;
        document.getElementById('menu-4').innerHTML = `<a href='#'>${data.item_4}</a>`;
        }
)
// new-host fetch
fetch(localApi+"new-host").then(
    function(res){
        return res.json();
    }
    ).then(
        function(datas) {
               
               var htmls = datas.post.map(
                function(item){
                    return `
                    <div class="host-item-${item.id}">
                    <img class="host-img" src="upload/host-${item.id}.jpg" alt="host-${item.id}">
                    </img>
                    <div class="host-content">
                    <div class="host-title">${item.category_name}</div>
                    <div class="host-description">
                    <div class="host-description-title">${item.title}</div>
                    <div class="host-description-time"> ${item.author}:${item.date}</div>
                    <div>${item.content}</div>
                    </div>
                    </div>
                    </div>`
                }
               );
               var htmlsPc = datas.post.filter(data => data.id > 1).map(
                function(item){
                    return `
                    <div class="pc-item-${item.id}">
                    <img class="host-img" src="upload/host-${item.id}.jpg" alt="host-${item.id}">
                    </img>
                    <div class="host-content">
                    <div class="host-title">${item.category_name}</div>
                    <div class="host-description">
                    <div class="host-description-title">${item.title}</div>
                    <div class="host-description-time"> ${item.author}:${item.date}</div>
                    <div>${item.content}</div>
                    </div>
                    </div>
                    </div>
                   `
                }
               );
              var htmlPc = htmlsPc.join('');
              var html = htmls.join('');
              document.getElementById('new-host').innerHTML = html
              document.getElementById('pc-host').innerHTML = htmlPc
            }
    )
    // fetch lastest-new
    fetch(localApi+"latest-new").then(
function(res){  
    return res.json();
}
).then(
    function(datas) {
        console.log(datas)
        var htmls = datas.post.map(
            function(item){
                return `
                <div class="news-item"> 
                <div class="news-item-left">
                <img class="news-item-img" src="upload/latest-${item.id-3}.jpg" alt="news" loading="lazy"></img>
                </div>
                <div class="news-item-content">
                <div class="news-item-content-tag">
                ${item.category_name}
                </div>
                <div class="news-item-content-title">
                <a href="#">${item.title}</a>
                </div>
                <div class="news-item-content-time">
                ${item.date}, by ${item.author}
                </div>
                <div class="news-item-content-desc">
                ${item.content}
                </div>
                </div>
                </div>
               `
            }
           );
           var html = htmls.join('')
           document.getElementById('latest-news').innerHTML = html
        }
)
// fetch popular-news
fetch(localApi+"popular-news").then(
    function(res){  
        return res.json();
    }
    ).then(
        function(datas) {
        
            var htmls = datas.post.map(
                function(item){
                    return `
                    <div class="popular-item"> 
                    <div class="popular-item-left">
                    <img class="popular-item-img" src="upload/${item.id}.jpg" alt="news" loading="lazy" ></img>
                    </div>
                    <div class="popular-item-content">
                    <div class="popular-item-content-tag">
                    ${item.category_name}
                    </div>
                    <div class="popular-item-content-title">
                    <a href="#">${item.title}</a>
                    </div>
                    </div>
                    </div>
                   `
                }
               );
               var html = htmls.join('')
               document.getElementById('popular-news').innerHTML = html
            }
    )
    // fetch tag
    fetch(localApi+"tag").then(
        function(res){  
            return res.json();
        }
        ).then(
            function(datas) {
                console.log(datas)
                var htmls = datas.map(
                    function(item){
                        return `
                        <div class="tag-item"> 
                        <img class="tag-icon" src="upload/tag.svg" style="color: #a6afbf;" loading="lazy"></img>
                        <span class="tag-desc">${item.tag}</span>
                        </div>
                       `
                    }
                   );
                   var html = htmls.join('')
                   document.getElementById('tag').innerHTML = html
                }
        )